# Galette2Dolibarr

Convertis un fichier CSV contenant les adhérents provenant du logiciel Galette
vers un fichier CSV compatible avec Dolibarr en vue d'un import.

**Note** : le fichier source provenant de Galette tel qu'attendu et tel que
présenté en exemple dans `data/galette_example.csv` a fait l'objet d'ajustements
pour présenter un adhérent par ligne.

## Prérequis

Ruby 3.2.2

## Installation

```
bundle install
```

## Usage

```
Commands:
  galette2dolibarr.rb membership [SUBCOMMAND]
```

```
Commands:
  galette2dolibarr.rb membership convert [CSVFILEPATH] [OUTPUT]                                # Convert Galette CSV to Dolibarr CSV
```

```
Command:
  galette2dolibarr.rb membership convert

Usage:
  galette2dolibarr.rb membership convert [CSVFILEPATH] [OUTPUT]

Description:
  Convert Galette CSV to Dolibarr CSV

Arguments:
  CSVFILEPATH                       # csvfile path
  OUTPUT                            # output filepath (defaults to stdout)

Options:
  --help, -h                        # Print this help
```

### Exemple

```
❯ bundle exec ./galette2dolibarr.rb membership convert data/galette_example.csv
```
