#!/usr/bin/env ruby
# frozen_string_literal: true

require "bundler/setup"
require "dry/cli"
require "csv"

module Galette2Dolibarr
  module CLI
    module Commands
      extend Dry::CLI::Registry

      module Membership
        class Convert < Dry::CLI::Command
          desc "Convert Galette CSV to Dolibarr CSV"

          argument :csvfilepath, desc: "csvfile path"
          argument :output, desc: "output filepath (defaults to stdout)"

          def call(csvfilepath:, output: "-", **)
            dcsv = DolibarrCSV.new(csvfilepath).call

            if output == "-"
              puts dcsv
            else
              IO.write(output, dcsv)
            end
          end
        end
      end

      register "membership" do |prefix|
        prefix.register "convert", Membership::Convert, aliases: ["c"]
      end
    end
  end

  # Build a DolibarrCSV object from a GaletteCSV object
  class DolibarrCSV
    def initialize(csvfilepath)
      @csvfilepath = csvfilepath
    end

    # Return the built DolibarrCSV object
    #
    # @return [CSV::Table] the DolibarrCSV object
    def call
      CSV.generate do |dolibarr_csv|
        dolibarr_csv << dolibarr_headers

        File.open(csvfilepath) do |galette_io|
          CSV.foreach(galette_io, headers: true, strip: true, converters: :all) do |galette_row|
            dolibarr_csv << transform_row(galette_row)
          end
        end
      end
    end

    private

    def dolibarr_headers
      [
        "Réf adhérent* (a.ref)",
        "Titre civilité (a.civility)",
        "Nom* (a.lastname)",
        "Prénom (a.firstname)",
        "Genre (a.gender)",
        "Identifiant* (a.login)",
        "Mot de passe (a.pass)",
        "Id type adhérent* (a.fk_adherent_type)",
        "Nature de l'adhérent* (a.morphy)",
        "Société (a.societe)",
        "Adresse (a.address)",
        "Code postal (a.zip)",
        "Ville (a.town)",
        "StateId|StateCode (a.state_id)",
        "CountryId|CountryCode (a.country)",
        "Tél pro. (a.phone)",
        "Tél perso. (a.phone_perso)",
        "Tél portable (a.phone_mobile)",
        "Email (a.email)",
        "Birthday (a.birth)",
        "État* (a.statut)",
        "Photo (a.photo)",
        "Note (publique) (a.note_public)",
        "Note (privée) (a.note_private)",
        "Date création (a.datec)",
        "Date fin adhésion (a.datefin)"
      ].freeze
    end

    def transform_row(row)
      civility, lastname, firstname = row["Nom :"].split(" ", 3)

      civility =
        case civility
        when "M", "M." then "MR"
        when /(Mme|Mlle)\.?/ then "MME"
        end

      gender =
        case row["Genre"]
        when "Homme" then "man"
        when "Femme" then "woman"
        end

      login = row["Identifiant :"] || "#{firstname[0]}#{lastname}"

      fk_adherent_type =
        case row["Genre"]
        when "Homme", "Femme" then 1
        when "Structure Associative" then 2
        else 1
        end

      morphy =
        case row["Genre"]
        when "Homme", "Femme" then "phy"
        when "Structure Associative" then "mor"
        else "phy"
        end

      societe = row["Nom"] if morphy == "mor"

      adresse = row["Adresse :"]
      zipcode = row["Code Postal :"]
      town = row["Ville :"]
      state_code = zipcode&.slice(0..2)
      country_code = row["Pays :"]&.slice(0...2)

      phone_pro = row["Tél :"] if morphy == "mor"
      phone_perso = row["Tél :"] if morphy == "phy"
      phone_mobile = row["GSM :"]
      email = row["Courriel :"]

      birthday = row["Date de naissance :"]

      status = row["COTISATION 2023"] =~ /oui/i ? 1 : 0

      note_public = row["Informations de contact :"]
      note_private = row["Autres informations (admin) :"]

      date_created = row["Date de création :"]
      date_membership = row["COTISATION 2023"].match(/\d{2}\/\d{2}\/\d{4}/)[0] if row["COTISATION 2023"]

      [
        "auto",                                            # Réf adhérent* (a.ref),
        civility,                                          # Titre civilité (a.civility),
        lastname.upcase,                                   # Nom* (a.lastname),
        firstname,                                         # Prénom (a.firstname),
        gender,                                            # Genre (a.gender),
        to_canonical(login),                               # Identifiant* (a.login),
        nil,                                               # Mot de passe (a.pass),
        fk_adherent_type,                                  # Id type adhérent* (a.fk_adherent_type),
        morphy,                                            # Nature de l'adhérent* (a.morphy),
        societe,                                           # Société (a.societe),
        adresse,                                           # Adresse (a.address),
        zipcode,                                           # Code postal (a.zip),
        town,                                              # Ville (a.town),
        state_code,                                        # StateId|StateCode (a.state_id),
        country_code.upcase,                               # CountryId|CountryCode (a.country),
        format_phone(phone_pro),                           # Tél pro. (a.phone),
        format_phone(phone_perso),                         # Tél perso. (a.phone_perso),
        format_phone(phone_mobile),                        # Tél portable (a.phone_mobile),
        email,                                             # Email (a.email),
        birthday,                                          # Birthday (a.birth),
        status,                                            # État* (a.statut),
        nil,                                               # Photo (a.photo),
        note_public,                                       # Note (publique) (a.note_public),
        note_private,                                      # Note (privée) (a.note_private),
        format_date(date_created),                         # Date création (a.datec),
        format_date(date_membership)&.public_send(:>>, 12) # Date fin adhésion (a.datefin)
      ].freeze
    end

    def format_phone(number)
      return if number.nil?

      formatted_number = number.to_s.tr("- ", "")
      formatted_number = "0#{formatted_number}" if formatted_number.size == 9
    end

    def format_date(date)
      return if date.nil?

      Date.strptime(date, "%d/%m/%Y")
    end

    def to_canonical(str)
      return "" if str.empty?

      str.unicode_normalize(:nfkd).chars.keep_if { |c| c =~ /\w/ }.join.downcase
    end

    attr_reader :csvfilepath
  end
end

Dry::CLI.new(Galette2Dolibarr::CLI::Commands).call
